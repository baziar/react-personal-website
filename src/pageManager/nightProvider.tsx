import { createCtxUseState } from "./stateManager";

export const [ModNight, NightProvider] = createCtxUseState(false);

export default NightProvider;
