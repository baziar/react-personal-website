import React from "react";
import "./activity.css";

function Activity() {
  return (
    <div className="activity">
      <div>
        <div className="activity-nav">
          <h2>{"With Tools"}</h2>
        </div>
        <br />
        <a className="activity-arrow">{">"}</a>
        {" Linux > Android > VSCode > Adobe > Git"}
        <br />
        <br />
      </div>

      <div>
        <div className="activity-nav">
          <h2>{"With Language"}</h2>
        </div>
        <br />
        <a className="activity-arrow">{">"}</a>
        {" Html > Css > Js > Ts > NodeJs > C/C++ > Java > Go > "}
        <br />
        <br />
      </div>

      <div>
        <div className="activity-nav">
          <h2>{"With Librarry"}</h2>
        </div>
        <br />
        <a className="activity-arrow">{">"}</a>{" "}
        {" React Js & Native > Apolo > GraphQl > webpack > Redux "}
        <br />
        <br />
      </div>

      <div>
        <div className="activity-nav">
          <h2>{"I'm Active @"}</h2>
        </div>
        <br />
        <a className="activity-arrow">{">"}</a>
        {" Web & Mobile App Development > FrontEnd > BackEnd > IOT "}
        <br />
        <br />
      </div>
    </div>
  );
}

export default Activity;
