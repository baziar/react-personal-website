export { default as Home } from "./home";
export { default as Smile } from "./smile";
export { default as Edu } from "./edu";
export { default as Activity } from './activity'
export { default as Coffee } from './coffee'