import React from "react";
import Html5 from "../../assets/html5.svg";
import Css from "../../assets/css.png";
import Sass from "../../assets/sass.svg";
import Javascript from "../../assets/javascript.svg";
import Node from "../../assets/nodejs.svg";
import Reactjs from "../../assets/reactjs.svg";
import Graphql from "../../assets/graphql.svg";
import Rest from "../../assets/rest.svg";
import Ts from "../../assets/typescript.svg";
import "./smile.css";

function Smile() {
  return (
    <div className="smile">

      <div className="smile-nav">
        <h2>{"I Smile @"}</h2>
      </div>

      <div className="smile-element">
        <img src={Html5} className="smile-img " alt="Html5" />
        <img src={Css} className="smile-img " alt="Css" />
        <img src={Javascript} className="smile-img " alt="Js" />
        <img src={Reactjs} className="smile-img " alt="React" />
        <img src={Ts} className="smile-img " alt="Ts" />
        <img src={Graphql} className="smile-img " alt="Gql" />
        <img src={Rest} className="smile-img " alt="Rest" />
        <img src={Sass} className="smile-img " alt="Sass" />
        <img src={Node} className="smile-img " alt="Node" />
      </div>
      
    </div>
  );
}

export default Smile;
