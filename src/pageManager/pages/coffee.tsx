import React from "react";
import "./coffee.css";

function Coffee() {
  const [coffe, setCoffe] = React.useState<boolean | undefined>(undefined);

  function SetCoffe(bool: boolean | undefined) {
    setCoffe(bool);
    console.log(coffe);
  }

  if (coffe === undefined) {
    return (
      <div className="coffee">
        <a>{"Can I Help You ? "}</a>

        <a onClick={() => SetCoffe(true)} className="coffee-arrow">
          {" True "}
        </a>
        {" : "}
        <a onClick={() => SetCoffe(false)} className="coffee-arrow">
          {" False "}
        </a>
      </div>
    );
  }

  return (
    <div className="coffee">
      <div>
        <br />
        {coffe ? (
          <div>
            <a className="coffee-arrow" onClick={() => SetCoffe(undefined)}>
              {"< True "}
            </a>
            <br /> <br />
            <a className="coffee-arrow">{">"}</a> {" Contact me to meet you "}
            <br />
            <a className="coffee-arrow">{"@"}</a> {"work or cafe"}
          </div>
        ) : (
          <div>
            <a className="coffee-arrow" onClick={() => SetCoffe(undefined)}>
              {"< False "}
            </a>
            <br /> <br />
            <a className="coffee-arrow">{">"}</a> {" Type your reason "}
            <br />
            <a className="coffee-arrow">{"@"}</a> {"below form"}
          </div>
        )}

        <br />
        <br />
      </div>
    </div>
  );
}

export default Coffee;
