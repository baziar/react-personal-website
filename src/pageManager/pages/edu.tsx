import React from "react";
import "./edu.css";

function Edu() {
  return (
    <div className="edu">
      <div className="edu-nav">
        <h2>{" My Education"}</h2>
      </div>

      <div>
        <br />
        <a className="edu-arrow">{">"}</a>
        {" Master's degree in telecommunication engineering"} <br />
        <br />
        <a className="edu-arrow">{"@"}</a> {"University of Isfahan (2013-2016)"}
        <br />
        <br />
        <br />
        <br />
        <a className="edu-arrow">{">"}</a>
        {" Bachelor's degree in electronics engineering"} <br />
        <br />
        <a className="edu-arrow">{"@"}</a> {"Lorestan University (2010-2012)"}
        <br />
        <br />
      </div>
    </div>
  );
}

export default Edu;
