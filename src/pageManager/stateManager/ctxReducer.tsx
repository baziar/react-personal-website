import React from "react";

export default function CreateCtx<State, Action>(
  reducer: React.Reducer<State, Action>,
  initialState: State
) {
  const defaultDispatch: React.Dispatch<Action> = () => initialState;

  const CTX = React.createContext({
    state: initialState,
    dispatch: defaultDispatch
  });

  function Provider(props: React.PropsWithChildren<{}>) {
    const [state, dispatch] = React.useReducer<React.Reducer<State, Action>>(
      reducer,
      initialState
    );
    return <CTX.Provider value={{ state, dispatch }} {...props} />;
  }
  return [CTX, Provider] as const;
}

